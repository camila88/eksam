<header class="blog-header py-3">

        <div class="row col-12 d-flex justify-content-end align-items-center">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">EST</a>
                            <div class="dropdown-menu" aria-labelledby="dropdown01">
                                <a class="dropdown-item" href="#">RUS</a>
                                <a class="dropdown-item" href="#">FIN</a>
                                <a class="dropdown-item" href="#">ENG</a>
                            </div>
                        </li>
                    </ul>
        </div>

    <div class="row flex-nowrap justify-content-end align-items-center">
        <div class="col-12 text-center">
            <a class="blog-header-logo" href="#"><img src="logo.png" height="110"></a>
        </div>
    </div>
</header>

<div class="nav-scroller py-1 mb-2">
    <nav class="nav d-flex justify-content-center">
        <div class="row">
        <a class="p-2 col-md-4 col-xs-12" href="#"><i class="fas fa-home"></i>AVALEHT</a>
        <a class="p-2 col-md-4 col-xs-12" href="#"><i class="fas fa-coffee"></i>KOHVIK</a>
        <a class="p-2 col-md-4 col-xs-12" href="#"><i class="fas fa-envelope"></i>KONTAKT</a>
        </div>
    </nav>
</div>
