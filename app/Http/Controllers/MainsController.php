<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Main;

class MainsController extends Controller
{
    public function show()
    {
        $mains = Main::all();
        return view('posts.index', compact( 'mains'));
    }

}

