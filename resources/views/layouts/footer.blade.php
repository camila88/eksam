<footer class="blog-footer">

    <div class="container">

        <div class="row">
            <div class="col-12 col-md">
                <h5>Lahtioleku ajad</h5>
                <ul class="list-unstyled text-small">
                    <li>E-R 10:00-14:00</li>
                    <li>L-P 12:00-18:00</li></br>

                    <h5>Jälgi meid</h5>
                    <li>
                        <div class="col-12 list-social ">
                            <a href="#"><i class="fab fa-snapchat-square"></i></a>
                            <a href="#"><i class="fab fa-instagram"></i></a>
                            <a href="#"><i class="fab fa-facebook-square"></i></a>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-12 col-md">
                <h5>Võta meiega ühendust</h5>
                <ul class="list-unstyled text-small">
                    <li>Lõõtsa 4, 11415 Tallinn, Eesti</li>
                    <li> (asume neljandal korrusel)</li></br>
                    <li>Tel. +372 735 0500</li>
                </ul>
            </div>
            <div class="col-12 col-md">
                <ul class="list-unstyled text-small">
                    <li><img src="map.png" height="120"></li>
                </ul>
            </div>
        </div>

</footer>
