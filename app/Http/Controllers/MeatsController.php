<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Meat;

class MeatsController extends Controller
{
    public function show()
    {
        $meats = Meat::all();
        return view('posts.index', compact( 'meats'));
    }

    public function store()
    {
        $this->validate(request(), [
            'name' =>'required',

        ]);

        Meat::create(request(['name']));

        return redirect('/');
    }

}

