@extends ('layouts.master')
@section ('content')

    @foreach ($mains as $main)
        <div class="col-md-4">
            <div class="card mb-4 box-shadow">
                <img class="card-img-top" src="{{$main->image_url}}" alt="Image">
                <div class="card-body">
                    <h5 class="card-title">{{$main->name}}</h5>
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                           <p> Lisa enda võileivale<a href="#"><i class="fas fa-plus-square"></i></a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

@endsection