<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">
    <title>Breadway</title>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <!-- Fontawesome styles for this template -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <!-- Custom styles for this template -->
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link href="css/blog.css" rel="stylesheet">

</head>
<body>

    <div class="container">
        @include('layouts.nav')
    </div><!-- /.container -->

    <main role="main" >

        <section class="jumbotron jumbotron-fluid text-center">
            <div class="container back-color">
                <h1 class="jumbotron-heading">Lihtne nagu leivategu!</h1>
                <p class="lead text-muted">Something short and leading about the collection below—its contents, the creator, etc. Make it short and sweet, but not too short so folks don't simply skip over it entirely.</p>
                <p>
                    <a href="#" class="btn btn-primary my-2">Vaata lähemalt</a>
                </p>
            </div>
        </section><!-- /.section -->

        <div class="container">

                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-main-tab" data-toggle="pill" href="#pills-main" role="tab" aria-controls="pills-main" aria-selected="true">Põhi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-meat-tab" data-toggle="pill" href="#pills-meat" role="tab" aria-controls="pills-meat" aria-selected="false">Liha</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-cheese-tab" data-toggle="pill" href="#pills-cheese" role="tab" aria-controls="pills-cheese" aria-selected="false">Juust</a>
                    </li>
                </ul>

                <div class="tab-content" id="pills-tabContent">
                    <div class=" tab-pane fade show active" id="pills-main" role="tabpanel" aria-labelledby="pills-main-tab">
                        <div class="row">
                            @yield ('content')
                        </div>
                    </div>

                    <div class=" tab-pane fade" id="pills-meat" role="tabpanel" aria-labelledby="pills-meat-tab">
                        <div class="row">
                            @yield ('content')
                        </div>
                    </div>

                    <div class="tab-pane fade" id="pills-cheese" role="tabpanel" aria-labelledby="pills-cheese-tab">
                        <div class="row">
                            @yield ('content')
                        </div>
                    </div>

                    <p>
                        <a href="#" class="btn btn-primary my-2">EDASI</a>
                    </p>
                </div>

        </div><!-- /.container -->

    </main><!-- /.main -->

    @include('layouts.footer')

</body>
</html>
