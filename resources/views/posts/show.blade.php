@extends ('layouts.master')

@section ('content')
    <div class="col-md-8 blog-main">
        <h2 class="blog-post-title">{{$post->title}}</h2>
        <p class="blog-post-meta">{{$post->created_at}} by <a href="#">Admin</a></p>
        <p>{{$post->content}}</p>
    </div>
@endsection

