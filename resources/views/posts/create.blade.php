@extends ('layouts.master')

@section ('content')
    <div class="col-md-8 blog-main">
        <h1>Create new post</h1>

        <hr>

        <form method="POST" action="/">

            {{csrf_field()}}

            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" name="name" >
            </div>

            <button type="submit" class="btn btn-primary">Vali</button>

        </form>
    </div>
@endsection

